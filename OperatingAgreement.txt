OPERATING AGREEMENT for DIOGENES HACKERSPACE, LLC

A Member-Managed Limited Liability Company

Version 0.3 06 September 2020

ARTICLE I

Company Formation

1.1 FORMATION

The Members hereby form a Limited Liability Company ("Company")
subject to the provisions of the Limited Liability Company Act as
currently in effect as of this date in Texas. Articles of Organization
shall be filed with the Secretary of State of Texas.

1.2 NAME

The name of the Company shall be Diogenes Hackerspace, LLC.

1.3 PURPOSE

The purpose of the Company is to provide the members of the company
shared resources to pursue individual projects and self-education,
provide members of the company shared resources to enable their own
individual businesses, and encourage and support education in useful
skills and technical knowledge among the general public, and any other
lawful business in furtherance of that purpose, or other lawful
business to their mutal benefit.

1.4 TERM

The Company shall continue for a perpetual period, or until the
unanimous consent of the Members to dissolve the Company, or until the
number of Members fails the requirement in 5.2.

1.5 PRINCIPAL PLACE OF BUSINESS

The location of the principal place of business of the Company shall
be:

13800 Dragline Dr., Suite C
Austin, Texas 78728

Principal place of business may be changed by the Members as described
in 4.5, 4.6, 4.7.

1.6 ENTITY FOR PURPOSES OF TAXATION

For purposes of taxation, the Company shall be operated as a "C
Corporation".  The Company will pay all taxes directly, filing IRS
Form 1120 or equivalent, and the Company will not file IRS K-1 Forms
for the Members.  The Members are each and solely responsible for
assuring that any distributions or other taxable benefits are reported
correctly on their individual taxes; note sections 3.2 and 3.3 limit
distributions.

1.7 MEMBERS OF THE COMPANY

The name and place of residence of each initial member are contained
in Exhibit 1 attached to this Agreement.

1.8 ADMISSION OF ADDITIONAL MEMBERS

Additional members may be admitted to the Company; an existing Member
must make a motion to admit the new Member, and receive a second and
the consent of the Members as described in 4.5, 4.6, and 4.7.

1.9 EXPULSION OF MEMBERS

A member may be expelled by vote of the Members as in 4.5, 4.6, and
4.7, however a 2/3 approval of all Members is required for explusion.
A Member proposing a motion to expel a Member must accompany it with a
$100 non-refundable capital contribution to the company.  The expelled
member forfeits their membership interest.  An expelled Member may not
be put forth for re-admission for 30 days.

1.10 WITHDRAWAL OF A MEMBER

A member may withdraw voluntarily by notice to the Members, or by
being 15 days late on payment of a Membership Fee.  A Member
withdrawing forfeits their Membership Interest.  Re-instatement
requires a current member put forth a motion as in 1.8.

1.10 REGISTERED AGENT

The name and location of the registered agent of the Company shall be:

Robert Ristroph
13800 Dragline Drive, Suite C
Austin, Texas 78728

ARTICLE II

Capital Contributions, Ownership Interest, Capital Accounts

2.1 INITIAL CONTRIBUTIONS

The Members initially shall contribute capital as described in the
Exhibit 1 attached to this Agreement.

2.2 ADDITIONAL CONTRIBUTIONS

Members may from time to time make additional contributions as
required to make motions to amend the Operating Agreement or expell
other Members or for special purposes such as raising additional
capital when there is shortfall.

2.3 MEMBERSHIP FEES

Members will pay an equal monthly usage fee, referred to as a
"Membership fee."  The Membership Fee will be equal for all members,
initially set at $100, and thereafter changed by vote of the Members
as described in 4.5, 4.6, and 4.7.  The Membership Fee may be set at
$0 if the Company does not need additional capital for a time.
Additional contributions in kind or in labor are separate from the
Membership Fee, do not reduce the Membership Fee, and are not required
to be equal.

2.4 OWNERSHIP INTEREST

Each Member's ownership interest shall be equal regardless of amount
of capital contribution.

2.5 CAPITAL ACCOUNTS

A single Capital Account called "Equity" will be kept reflecting all
Capital Contributions as well as retained earnings.  A Member's
Capital Account will be that total Equity divided by the number of
members, and documentation of that number will be provided to Member's
who need to document a Capital Account for any reason.  Note
distribution of the Capital Account is limited in 3.2.

ARTICLE III

Profits, Losses, and Distributions

3.1 PROFITS/LOSSES

For financial accounting and tax purposes the Company's net profits or
net losses shall be determined as needed to pay taxes.

3.2 DISTRIBUTIONS

There shall be no distributions except at liquidation or winding up.
Members will derive benefits from access and use of the shared
facilities and resources maintained by the Company, and not via direct
distribution.  When a Member derives a taxable benefit from their
Membership, it is that individual Member's sole responsibility to
report and pay all taxes.

3.3 DISTRIBUTIONS AT LIQUIDATION

Distributions in liquidation of the Company shall be made in
accordance with the ownership interest at time of liquidation, after
satisfying all Company creditors, other obligations, and taxes.  If a
Member fails to receive their distribution, by not cashing a check or
being unreachable, for a period of 90 days, that Member forfeits their
Distribution, and such Distribution is to be rendered into United
States Currency and given to Hobos underneath the I-35 bridge at 7th
Street in Austin, Texas; the purpose of this provision is to assure
the winding up process not become an albatross on the neck of the last
Members.


ARTICLE IV

Management

4.1 MANAGEMENT OF THE BUSINESS

The Members shall collectively manage the company. The members by
majority vote may elect Officers to act as fully empowered agents of
the company, for terms of 1 year, and designate limits on the spending
authority of those Officers as needed.  If no such Officers have been
elected, each Member shall have to authority to act as a fully
empowered agent of the Company.

4.2 LIMITS OF THE INDIVIDUAL MEMBERS

Article 4.1 notwithstanding, no Member shall contract the Company,
make an expenditure, secure or grant a loan, or otherwise expose,
commit, or obligate the Company for any amount exceeding $50 without
the consent of the Members as described in 4.5, 4.6, 4.7. This
provision is cumulative, such that the limitation of this provision
cannot be obviated by breaking a larger action into a series of
smaller actions.  If Members elect an Officer, the Members will
designate limits on their authority at the time of election.

A Member who obligates the Company beyond these limits without
approval of the Membership, to an outside party that might reasonably
expect the Member had such authority, bears full responsibility for
the full amount of the obligation to the Company.

4.3 RECORDS

The Members shall cause the Company to keep the following:

(a) A current list of the full name and the last known street address
of each Member;

(b) A copy of the Certificate of Formation and the Company Operating
Agreement and all amendments;

(c) Copies of the Company's federal, state and local income tax
returns and reports, if any, for the three most recent years;

(d) Copies of any financial statements of the limited liability
company for the three most recent years;

(e) Minutes of all meetings and records of votes;

(f) A current copy of this Operating Agreement, and any additional
policies or bylaws binding the Members.

4.4 MODIFICATIONS TO THE OPERATING AGREEMENT

Modifications to this Operating Agreement shall be presented to the
Members with a second 30 days before a vote is calculated, and require
a vote of 2/3 majority to be in effect, calculated as in 4.7 a) .  A
Member presenting a modification to this Operating Agreement must
accompany it with an additional $100 capital contribution to the
Company, which is not refundable, regardless of whether the proposed
modification is accepted or not.

4.5 MEETINGS

Meetings in person may be regularly scheduled at times and places
approved by the members.  The time and place of a meeting must be
announced 30 days before the meeting, and an agenda provided to the
members 7 days before the meeeting.  New business not on the agenda
may be discussed but only voted on at a subsequent meeting or as in
4.7 "ACTIONS WITHOUT A MEETING" .

A quorum of a regularly scheduled meeting shall consist of 2/3 of
Members.  Without quorum a meeting may proceed, but all questions must
be voted on as in 4.7 "ACTIONS WITHOUT A MEETING", and thus cannot
take effect immediately.

Members may be represented at a meeting by proxy, as long as the
person holding the proxy can present a written assignment of proxy.
For a question to pass at a regularly scheduled in-person meeting, a
simple majority ( 50% ) of all members (not just those at the meeting
physically or via proxy) must approve.  Questions failing may be
referred to the membership for voting via 4.7 "ACTIONS WITHOUT A
MEETING."

4.6 EMERGENCY MEETINGS

A Member may call an emergency meeting, which shall follow the
procedure of 4.5 "MEETINGS" but not require 30 days notice.  The
Member calling such an emergency meeting shall make a $100 capital
contribution, which is not refundable regardless of whether a quorum
attends the meeting or not.

4.7 ACTIONS WITHOUT A MEETING

Members may make proposals and vote electronically without a meeting.
Proposals must receive a second to be voted on.

a) The fraction of approving Members for a question is calculated by
number of Ayes divided by the number of Members not Abstaining.

Approval Fraction = Num. of Ayes / ( Num. of Members - Num. of Abstentions )

The phrase "Approval of a majority of the Members" means the Approval
Fraction is 1/2.  The phrase "2/3 Members approval" means the Approval
Fraction is 2/3.

b) If a proposal receives unanimous consent it is adopted immediately.

c) If a proposal has 2/3 Members approval 8 days after receiving a
second, it is adopted at that time.

d) If a proposal has a majority ( 1/2 ) of Members approval 15 days
after receiving a second, it is adopted at that time.

e) A proposal failing to acheive a majority ( 1/2 ) of Members
approval 15 days after receiving a second is removed from the table,
and cannot be re-introduced for 30 days.


4.8 NO EMPLOYEES, NO MEMBER CONTRACTORS

The Company will not employ any person. Members may not be hired as
contractors by the Company.  Work or services done by Members can only
be a capital contribution.

4.9 FREEDOM OF COMMUNICATION

No Company resource such as a mailing list server or anything else
shall be used in such a way as to censor or moderate communications of
the members to each other, the Free communication of Members being
essential to the good governance of the Company.  The remedy for
persistent or volumnous communication by a Member, or other abuse of
Company communication resources such as a mailing list, is limited to
expelling the Member as in 1.9.

4.10 RETAINING PROFESSIONAL SERVICES

If the Company retains the services of an attorney, accountant, or
other licensed professional, such services and consulation will be in
writing and open to inspection by all members, or recorded
electronically and available to all members.  The Company will never
have reason to have one part of the Membership consult an attorney or
other professional in secret from another, because the Company will
never be in a dispute with any of it's Members, since the Members may
eject a disagreeable individual Member as in 1.9 before proceeding
with any such consultation.  An individual Member consulting with any
Professional outside of these terms does so on their own sole behalf,
and not as a representive or agent of the Company, and Company
resources or money may not be used to pay for such individually
retained services.

ARTICLE V

Winding Up

5.1 VOLUNTARY WINDING UP

By unanimous consent the members may wind up the company.

5.2 MINIMUM MEMBERSHIP

If the number of members should be less than 6 for 90 days, the
Company will be wound up, dissolved, and liquidated.

ARTICLE VI

Transfers

6.1 NON-TRANSFERABILITY

Membership interest is not transferable.

ARTICLE VII

Boilerplate

7.1 Severability

If any provision of thie Agreement is held illegal or unenforceable in
a judicial proceeding, such a provision shal be severed and shall be
non-operational, and the remainder of this Agreement shall remain
operational and binding on the Members.

7.2  Headings Have No Significance

The headings contained in this Agreement are for reference purposes
only and shall not affect in any way the meaning and interpretation of
this Agreement.
